using SuiteBot3.Game;

namespace SuiteBot3.Test.Game
{
	[TestClass]
	public class PointTest
	{
		[TestMethod]
		public void XShouldHaveTheValueSetInTheConstructor()
		{
			const int X = 13;

			Point point = new Point(X, 17);

			Assert.AreEqual(X, point.X);
		}

		[TestMethod]
		public void YShouldHaveTheValueSetInTheConstructor()
		{
			const int Y = 19;

			Point point = new Point(11, Y);

			Assert.AreEqual(Y, point.Y);
		}

		[TestMethod]
		public void SamePointsShouldEqual()
		{
			Assert.AreEqual(new Point(1, 1), new Point(1, 1));
		}

		[TestMethod]
		public void DifferentPointsShouldNotEqual()
		{
			Assert.AreNotEqual(new Point(1, 1), new Point(1, 2));
			Assert.AreNotEqual(new Point(1, 1), new Point(2, 1));
		}
	}
}
