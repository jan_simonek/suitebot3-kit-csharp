using Microsoft.VisualStudio.TestTools.UnitTesting;
using SuiteBot3.Game;
using SuiteBot3.Json;

namespace SuiteBot3.Test.Json
{
	[TestClass]
	public class JsonUtilTest
	{
		public const string MovesMessage =
			"{" +
					"\"messageType\":\"moves\"," +
					"\"moves\":[{\"playerId\":1,\"move\":\"FIRE\"},{\"playerId\":2,\"move\":\"JUMP\"}]" +
			"}";

		public const string SetupMessage =
			"{" +
					"\"messageType\":\"setup\"," +
					"\"aiPlayerId\":1," +
					"\"players\":[{\"id\":2,\"name\":\"R2D2\"},{\"id\":1,\"name\":\"C-3PO\"}]," +
					"\"gamePlan\":{" +
						"\"width\":10," +
						"\"height\":20," +
						"\"startingPositions\":[{\"x\":2,\"y\":2},{\"x\":7,\"y\":7}]," +
						"\"maxRounds\":25" +
					"}" +
			"}";

		[TestMethod]
		public void TestDecodeMessageType()
		{
			Assert.AreEqual(JsonUtil.MessageType.Moves, JsonUtil.DecodeMessageType(MovesMessage));
			Assert.AreEqual(JsonUtil.MessageType.Setup, JsonUtil.DecodeMessageType(SetupMessage));
		}

		[TestMethod]
		public void TestDecodeMovesMessage()
		{
			GameRound gameRound = JsonUtil.DecodeMovesMessage(MovesMessage);

			Assert.AreEqual(2, gameRound.Moves.Count);
			Assert.AreEqual(1, gameRound.Moves[0].PlayerId);
			Assert.AreEqual("FIRE", gameRound.Moves[0].Move);
			Assert.AreEqual(2, gameRound.Moves[1].PlayerId);
			Assert.AreEqual("JUMP", gameRound.Moves[1].Move);
		}

		[TestMethod]
		public void TestDecodeSetupMessage()
		{
			GameSetup gameSetup = JsonUtil.DecodeSetupMessage(SetupMessage);

			Assert.AreEqual(1, gameSetup.AiPlayerId);
			Assert.AreEqual(2, gameSetup.Players[0].Id);
			Assert.AreEqual("R2D2", gameSetup.Players[0].Name);
			Assert.AreEqual(1, gameSetup.Players[1].Id);
			Assert.AreEqual("C-3PO", gameSetup.Players[1].Name);
			Assert.AreEqual(10, gameSetup.GamePlan.Width);
			Assert.AreEqual(20, gameSetup.GamePlan.Height);
			Assert.AreEqual(new Point(2, 2), gameSetup.GamePlan.StartingPositions[0]);
			Assert.AreEqual(new Point(7, 7), gameSetup.GamePlan.StartingPositions[1]);
			Assert.AreEqual(25, gameSetup.GamePlan.MaxNumberOfGameRounds);
		}
	}
}
