using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SuiteBot3.Server;

namespace SuiteBot3.Test.Server
{
	[TestClass]
	public class SimpleServerTest
	{
		private class ToLowerCaseConverter : ISimpleRequestHandler
		{
			public string ProcessRequest(string request)
			{
				return request.ToLower();
			}
		}

		private const int Port = 4444;

		[TestInitialize]
		public void Setup()
		{
			StartServer();
		}

		[TestCleanup]
		public void Teardown()
		{
			if (ServerThread.IsAlive)
				ShutdownServer();

			WaitForServerToShutdown(1000);
		}

		[TestMethod]
		public void TestRequestResponse()
		{
			Assert.AreEqual("foobar", RequestServerResponse("FooBar"));
			Assert.AreEqual("nextrequest", RequestServerResponse("NextREQUEST"));
		}

		[TestMethod]
		public void TestShuttingDown()
		{
			Assert.IsTrue(ServerThread.IsAlive);

			ShutdownServer();

			WaitForServerToShutdown(1000);

			Assert.IsFalse(ServerThread.IsAlive);
		}

		[TestMethod]
		public void TestUptimeRequest()
		{
			Thread.Sleep(1500);

			int uptime = Convert.ToInt32(RequestServerResponse(SimpleServer.UptimeRequest));

			Assert.IsTrue(uptime > 0);
		}

		private string RequestServerResponse(string request)
		{
			TcpClient client = new TcpClient("localhost", Port);

			NetworkStream stream = client.GetStream();

			string response = null;

			using (StreamReader reader = new StreamReader(stream))
			{
				using (StreamWriter writer = new StreamWriter(stream))
				{
					writer.WriteLine(request);

					writer.Flush();

					response = reader.ReadLine();
				}
			}

			stream.Close();
			client.Close();

			return response;
		}

		private void ShutdownServer()
		{
			RequestServerResponse(SimpleServer.ShutdownRequest);
		}

		private void StartServer()
		{
			ServerThread = new Thread(new ThreadStart(StartSimpleServer));

			ServerThread.Start();

			// Give the server thread some time to initialize.
			Thread.Sleep(100);
		}

		private void StartSimpleServer()
		{
			SimpleServer server = new SimpleServer(Port, new ToLowerCaseConverter());

			server.Run();
		}

		private void WaitForServerToShutdown(int maxWaitTimeMilliseconds)
		{
			int elapsed = 0;

			while (ServerThread.IsAlive)
			{
				if (elapsed > maxWaitTimeMilliseconds)
					throw new TimeoutException("Timed out while waiting for the server to shutdown.");

				Thread.Sleep(10);

				elapsed += 10;
			}
		}

		private Thread ServerThread { get; set; }
	}
}
