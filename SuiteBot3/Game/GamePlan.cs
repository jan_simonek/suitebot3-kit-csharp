using System;
using System.Collections.Generic;
using System.Linq;

namespace SuiteBot3.Game
{
	public class GamePlan
	{
		public GamePlan(int width, int height, IEnumerable<Point> startingPositions, int maxNumberOfGameRounds)
		{
			Height = height;
			MaxNumberOfGameRounds = maxNumberOfGameRounds;
			StartingPositions = startingPositions.ToList();
			Width = width;
		}

		public int Height { get; }

		public int MaxNumberOfGameRounds { get; }

		public List<Point> StartingPositions { get; }

		public int Width { get; }

		public void ChecksIsOnPlan(Point position)
		{
			throw new NotImplementedException();
		}

		public IDictionary<int, Point> GetHomeBasePositions()
		{
			throw new NotImplementedException();
		}
	}
}
