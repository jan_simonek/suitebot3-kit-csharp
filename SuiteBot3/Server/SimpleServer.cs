using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace SuiteBot3.Server
{
	public class SimpleServer
	{
		public const string ShutdownRequest = "EXIT";

		public const string UptimeRequest = "UPTIME";

		public SimpleServer(int port, ISimpleRequestHandler requestHandler)
		{
			Port = port;
			RequestHandler = requestHandler;
			ShouldShutdown = false;
			Stopwatch = new Stopwatch();
		}

		public void Run()
		{
			Stopwatch.Restart();

			TcpListener listener = null;
			TcpClient client = null;

			try
			{
				listener = new TcpListener(IPAddress.Any, Port);

				listener.Start();

				while (!ShouldShutdown)
				{
					client = listener.AcceptTcpClient();

					HandleRequest(client);
				}
			}
			finally
			{
				client?.Close();
				listener?.Stop();
			}
		}

		private void HandleRequest(TcpClient client)
		{
			var stream = client.GetStream();

			using (var reader = new StreamReader(stream))
			{
				var request = reader.ReadLine();

				if (string.IsNullOrWhiteSpace(request))
					return;

				
				if (request == ShutdownRequest)
				{
					ShouldShutdown = true;

					return;
				}

				using (var writer = new StreamWriter(stream))
				{
					if (request == UptimeRequest)
						writer.WriteLine(Convert.ToInt32(Stopwatch.Elapsed.TotalSeconds));
					else
						writer.WriteLine(RequestHandler.ProcessRequest(request));
				}
			}

			stream.Close();
		}

		private int Port { get; }

		private ISimpleRequestHandler RequestHandler { get; }

		private bool ShouldShutdown { get; set; }

		private Stopwatch Stopwatch { get; }
	}
}
